import {
  HttpClient,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { StorageService } from '../core/services/storage.service';

@Injectable({
  providedIn: 'root'
})

export class TokenInterceptorService implements HttpInterceptor {
  private userSubscription: Subscription;
  constructor(
    private readonly storage: StorageService,
    private readonly http: HttpClient
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    const token = this.storage.get('token') || '';
    const updatedRequest = request.clone({
      headers: request.headers.set('Authorization', `Bearer ${token}`)
    });

    return next.handle(updatedRequest);
  }
}
