import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { AuthService } from './services/auth.service';
import { JwtService } from './services/jwt.service';
import { NotificatorService } from './services/notificator.service';
import { StorageService } from './services/storage.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, ToastrModule.forRoot()],
  providers: [StorageService, NotificatorService, AuthService, JwtService]
})
export class CoreModule {}
