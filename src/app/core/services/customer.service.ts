import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer } from 'src/app/common/interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private readonly http: HttpClient) { }

  getAllCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(`http://localhost:3000/customers`);
  }

  getSingleCustomer(customerId: string): Observable<Customer> {
    return this.http.get<Customer>(
      `http://localhost:3000/customer/${customerId}`
    );
  }

  createCustomer(brandName: string): Observable<Customer> {
    return this.http.post<Customer>(`http://localhost:3000/customer`, {
      brandName
    });
  }

  editCustomer(customerId: string, newCustomerName: string) {

    return this.http.put<Customer>(
      `http://localhost:3000/customer/${customerId}`,
      {
        brandName: newCustomerName
      }
    );
  }

  deleteCustomer(customerId: string) {
    return this.http.delete<Customer>(
      `http://localhost:3000/customer/${customerId}`
    );
  }
}
