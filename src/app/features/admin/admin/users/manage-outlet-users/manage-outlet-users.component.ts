import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer } from 'src/app/common/interfaces/customer';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { RegisterUser } from 'src/app/common/interfaces/register-user';
import { User } from 'src/app/common/interfaces/user';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { OutletService } from 'src/app/core/services/outlet.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-manage-outlet-users',
  templateUrl: './manage-outlet-users.component.html',
  styleUrls: ['./manage-outlet-users.component.css']
})
export class ManageOutletUsersComponent implements OnInit {
  currentCustomer: Customer;
  allCustomerOutlets: Outlet[];
  currentOutlet: Outlet;
  outletUsers: User[];
  currentUserId: string;
  currentCustomerBrand = '';
  currentOutletLocation = '';

  constructor(
    private readonly route: ActivatedRoute,
    private readonly customerService: CustomerService,
    private readonly outletService: OutletService,
    private readonly userService: UserService,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.outletUsers = data.outlet_users;
    });
    this.route.paramMap.subscribe(data => {
      const customerId: string = data.get('customerId');
      const outletId: string = data.get('outletId');

      // get the customer
      this.customerService.getSingleCustomer(customerId).subscribe(
        customer => {
          this.currentCustomer = customer;
          this.currentCustomerBrand = customer.brand;
        },
        error => {
          this.notificator.error('There was an error! Please try again or contact our administrator!');
        }
      );

      // get the outlet
      this.outletService.getCustomerOutlets(customerId).subscribe(
        outlets => {
          this.allCustomerOutlets = outlets;
          this.currentOutlet = outlets.find(outlet => outlet.id === outletId);
          this.currentOutletLocation = this.currentOutlet.location;
        },
        error => {
          this.notificator.error('There was an error! Please try again or contact our administrator!');
        }
      );

      this.userService.getAllUsersForOutlet(customerId, outletId).subscribe(
        users => {
          this.outletUsers = users;
        },
        error => {
          this.notificator.error('There are no users yet!');
        }
      );
    });
  }

  registerUser(userToRegister: RegisterUser) {
    return this.userService
      .registerBasicUser(
        this.currentCustomer.id,
        this.currentOutlet.id,
        userToRegister
      )
      .subscribe(
        (data: User) => {
          this.outletUsers.push(data);
          this.notificator.success('Register success!');
        },
        error => {
          this.notificator.error('Incorrect credentials! Please try again!');
        }
      );
  }

  getCurrentUserId(userId: string) {
    this.currentUserId = userId;
  }

  editUserOutlet(newUserObject: string) {
    const currentUser = this.outletUsers.find(
      user => user.id === this.currentUserId
    );
    const newOutletEntity = this.allCustomerOutlets.find(
      outlet => outlet.location === newUserObject
    );
    this.userService
      .changeUserOutlet(
        this.currentCustomer.id,
        this.currentOutlet.id,
        currentUser.id,
        newOutletEntity.id
      )
      .subscribe(
        response => {
          this.notificator.success('Customer Editted');
          this.outletUsers.map(user =>
            user.id === currentUser.id
              ? (user.outletName.location = newUserObject)
              : (user.outletName.location = user.outletName.location)
          );
          const userMapped = this.outletUsers.findIndex(
            user => user.id === currentUser.id
          );
          this.outletUsers.splice(userMapped, 1);
        },
        error => {
          this.notificator.error('There was an error! Please try again or contact our administrator!');
        }
      );
  }

  deleteUser(userId: string) {
    this.userService
      .deleteUser(this.currentCustomer.id, this.currentOutlet.id, userId)
      .subscribe(response => {
        this.notificator.success('User Deleted');
        const userIndex = this.outletUsers.findIndex(
          user => user.id === userId
        );
        this.outletUsers.splice(userIndex, 1);
      });
  }
}
