import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayOutletUsersComponent } from './display-outlet-users.component';

describe('DisplayOutletUsersComponent', () => {
  let component: DisplayOutletUsersComponent;
  let fixture: ComponentFixture<DisplayOutletUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayOutletUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayOutletUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
