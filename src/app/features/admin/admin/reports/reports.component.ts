import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Activity } from 'src/app/common/interfaces/activity';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  redemptionRecords: RedemptionRecord[];
  activityRecords: Activity[];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      data => {
        this.redemptionRecords = data.redemption_records;
        this.activityRecords = data.activity_records;
      },
      error => {
        this.notificator.error('There was an error! Please try again or contact our administrator!');
      }
    );
  }
}
