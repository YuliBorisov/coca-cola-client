import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Activity } from 'src/app/common/interfaces/activity';

@Component({
  selector: 'app-audit-trail',
  templateUrl: './audit-trail.component.html',
  styleUrls: ['./audit-trail.component.css'],
  providers: [DatePipe]
})
export class AuditTrailComponent implements OnInit {
  @Input()
  activityRecords: Activity[];

  activityRecords$: Observable<Activity[]>;
  filter = new FormControl('');

  private p = 1;


  constructor(private readonly pipe: DatePipe) { }

  ngOnInit() {
    this.activityRecords$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text, this.pipe))
    );
  }

  search(text: string, pipe: PipeTransform): Activity[] {
    return this.activityRecords.filter(activity => {
      const term = text.toLowerCase();

      return (
        activity.modifyingUserName.toLowerCase().includes(term) ||
        activity.actionType.toLowerCase().includes(term) ||
        activity.featureType.toLowerCase().includes(term) ||
        activity.itemName.toLowerCase().includes(term) ||
        pipe
          .transform(activity.createdOn)
          .toLowerCase()
          .includes(term)
      );
    });
  }

}
