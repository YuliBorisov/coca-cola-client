import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Admin } from 'src/app/common/interfaces/admin';
import { Customer } from 'src/app/common/interfaces/customer';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { RegisterUser } from 'src/app/common/interfaces/register-user';
import { User } from 'src/app/common/interfaces/user';
import { AdminService } from 'src/app/core/services/admin.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { OutletService } from 'src/app/core/services/outlet.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-manage-all-users',
  templateUrl: './manage-all-users.component.html',
  styleUrls: ['./manage-all-users.component.css']
})
export class ManageAllUsersComponent implements OnInit {
  admins: Admin[];
  users: User[];
  customers: Customer[];
  outlets: Outlet[];
  currentCustomer;
  currentOutlet;
  currentUserId;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly notificator: NotificatorService,
    private readonly adminService: AdminService,
    private readonly outletService: OutletService,
    private readonly userService: UserService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => (this.admins = data.admins));
    this.route.data.subscribe(data => (this.users = data.users));
    this.route.data.subscribe(data => (this.customers = data.customers));
  }

  getCurrentUserIdToDelete(id: string) {
    this.currentUserId = id;
  }

  registerAdmin(admin: RegisterUser) {
    this.adminService.registerAdmin(admin).subscribe(
      (data: Admin) => {
        this.admins.push(data);
        this.notificator.success('Register success!');
      },
      error => {
        this.notificator.error('Wrong credentials! Please try again!');
      }
    );
  }

  deleteAdmin(adminId: string) {
    this.adminService.deleteAdmin(adminId).subscribe(
      data => {
        const adminToRemove = this.admins.filter(admin => admin.id === adminId);
        this.admins.splice(this.admins.indexOf(adminToRemove[0]), 1);
        this.notificator.success('You have successfuly deleted this admin');
      },
      error => {
        this.notificator.error(
          'There was a problem with this action! Please contact our administrator!'
        );
      }
    );
  }

  getCustomerByName(name: string) {
    this.currentCustomer = this.customers.filter(
      customerName => customerName.brand === name
    );
    this.outletService
      .getCustomerOutlets(this.currentCustomer[0].id)
      .subscribe(data => {
        this.outlets = data;
      });
  }

  getOutletByName(name: string) {
    this.currentOutlet = this.outlets.filter(
      outletName => outletName.location === name
    );
  }

  registerBasicUser(user: RegisterUser) {
    const customerId = this.currentCustomer[0].id;
    const outletId = this.currentOutlet[0].id;
    this.userService.registerBasicUser(customerId, outletId, user).subscribe(
      (data: User) => {
        this.users.push(data);
        this.notificator.success('Register success!');
      },
      error => {
        this.notificator.error('Wrong credentials! Please try again!');
      }
    );
  }

  deleteUser(userId: string) {
    const user = this.users.find(x => x.id === userId);
    const outletId = user.outletName.id;
    const customerId = user.brandName.id;
    this.userService
      .deleteUser(customerId, outletId, userId)
      .subscribe(data => {
        const userToRemove = this.users.filter(
          userToDel => userToDel.id === userId
        );
        this.users.splice(this.users.indexOf(userToRemove[0]), 1);
      });
  }
}
