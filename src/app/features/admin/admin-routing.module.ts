import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivityResolverService } from './activity-resolver.service';
import { AdminResolverService } from './admin-resolver.service';
import { AdminComponent } from './admin/admin.component';
import { ManageCustomersComponent } from './admin/customer/manage-customers/manage-customers.component';
import { ManageSingleCustomerComponent } from './admin/customer/manage-single-customer/manage-single-customer.component';
import { ReportsComponent } from './admin/reports/reports.component';
import { ManageOutletUsersComponent } from './admin/users/manage-outlet-users/manage-outlet-users.component';
import { CustomerResolveService } from './customer-resolver.service';
import { ManageAdminComponent } from './manage-all-users/manage-admin/manage-admin.component';
import { ManageAllUsersComponent } from './manage-all-users/manage-all-users.component';
import { ManageUsersComponent } from './manage-all-users/manage-users/manage-users.component';
import { OutletResolverService } from './outlet-resolver.service';
import { OutletUsersResolverService } from './outlet-users-resolver.service';
import { RedemptionRecordResolver } from './redemption-record-resolver.service';
import { UserResolverService } from './user-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    pathMatch: 'full'
  },
  {
    path: 'manage-customers',
    component: ManageCustomersComponent,
    resolve: { all_customers: CustomerResolveService }
  },
  {
    path: 'manage-customers/:customerId',
    component: ManageSingleCustomerComponent,
    resolve: { all_customer_outlets: OutletResolverService }
  },
  {
    path: 'manage-customers/:customerId/outlets/:outletId',
    component: ManageOutletUsersComponent,
    resolve: { outlet_users: OutletUsersResolverService }
  },
  {
    path: 'manage-users',
    component: ManageAllUsersComponent,
    resolve: {
      admins: AdminResolverService,
      users: UserResolverService,
      customers: CustomerResolveService,
      outlet: OutletResolverService
    }
  },
  {
    path: 'manage-users/admin',
    component: ManageAdminComponent,
    resolve: { admins: AdminResolverService }
  },
  {
    path: 'manage-users/user',
    component: ManageUsersComponent,
    resolve: { users: UserResolverService }
  },
  {
    path: 'reports',
    component: ReportsComponent,
    resolve: {
      redemption_records: RedemptionRecordResolver,
      activity_records: ActivityResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
