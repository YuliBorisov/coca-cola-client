import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserResolverService } from '../admin/user-resolver.service';
import { BarcodeScannerComponent } from './barcode-scanner/barcode-scanner.component';
import { PrizeBarcodeScannerComponent } from './prize-barcode-scanner/prize-barcode-scanner.component';
import { UserHistoryComponent } from './user-history/user-history.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserComponent } from './user.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    pathMatch: 'full',
    resolve: { users: UserResolverService }
  },
  {
    path: 'barcode-scan',
    component: BarcodeScannerComponent,
    pathMatch: 'full'
  },
  {
    path: 'barcode-scan/:barcode',
    component: PrizeBarcodeScannerComponent,
    pathMatch: 'full'
  },
  {
    path: 'profile',
    component: UserProfileComponent,
    resolve: {
      users: UserResolverService
    }
  },
  {
    path: 'history',
    component: UserHistoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
