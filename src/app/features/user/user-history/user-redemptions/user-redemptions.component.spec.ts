import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRedemptionsComponent } from './user-redemptions.component';

describe('UserRedemptionsComponent', () => {
  let component: UserRedemptionsComponent;
  let fixture: ComponentFixture<UserRedemptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRedemptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRedemptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
