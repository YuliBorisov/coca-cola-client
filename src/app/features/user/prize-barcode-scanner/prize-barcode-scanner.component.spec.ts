import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizeBarcodeScannerComponent } from './prize-barcode-scanner.component';

describe('PrizeBarcodeScannerComponent', () => {
  let component: PrizeBarcodeScannerComponent;
  let fixture: ComponentFixture<PrizeBarcodeScannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrizeBarcodeScannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeBarcodeScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
