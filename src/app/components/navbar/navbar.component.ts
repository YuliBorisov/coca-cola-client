import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input()
  public loggedIn: boolean;
  @Input()
  public username: string;
  @Input()
  public role: string;

  @Output()
  public emitSearch = new EventEmitter<string>();
  @Output()
  public emitLogout = new EventEmitter<undefined>();

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService
  ) { }

  ngOnInit() {
  }

  public triggerLogout() {
    this.emitLogout.emit();
  }

  homeNavigate(role: string) {
    // const role = this.authService.role;
    if (role === 'Admin') {
      this.router.navigate(['admin']);
    } else {
      this.router.navigate(['user']);
    }
  }

}
