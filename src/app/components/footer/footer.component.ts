import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  @Output()
  emitIssue = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  submitIssue(issueObj: { name: string; email: string; issue: string }) {
    console.log('footer');
    console.log(issueObj);

    this.emitIssue.emit(issueObj);
  }
}
