export interface Outlet {
  id: string;
  location: string;
  createdOn: Date;
}
