import { Customer } from './customer';
import { Outlet } from './outlet';
import { Prize } from './prize';
import { User } from './user';

export interface RedemptionRecord {
  id: string;

  barcode: string;

  brand: Customer;

  outlet: Outlet;

  prize: Prize;

  user: User;

  timestamp: Date;
}
