export interface Customer {
  id: string;
  brand: string;
  createdOn: Date;
}
