export enum PrizeType {
  WhiteTeddyBear = 'Teddy Bear, White',
  BrownTeddyBear = 'Teddy Bear, Brown',
  'Coca-Cola' = 'Coca-Cola, 500 ml',
  Sprite = 'Sprite, 500 ml',
  Fanta = 'Fanta, 500 ml',
  Frisbee = 'Frisbee'
}
